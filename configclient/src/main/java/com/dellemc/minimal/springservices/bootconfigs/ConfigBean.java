package com.dellemc.minimal.springservices.bootconfigs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@RefreshScope
@Component
public class ConfigBean {
    @Value("${my.config.dynamicprop:Config Server is not working..pelase check}")
    String dynamicProp;

    @Value("${my.config.staticprop:No my.config.staticprop defined in application.yml}")
    String staticProp;

    public String getDynamicProp() {
        return dynamicProp;
    }

    public String getStaticProp() {
        return staticProp;
    }
}
