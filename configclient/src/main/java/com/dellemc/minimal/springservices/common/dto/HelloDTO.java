package com.dellemc.minimal.springservices.common.dto;

public class HelloDTO {
    public String customer = "Customer";
    public String special = "Pizza";
    public String sauce = "no sauce";

    @Override
    public String toString() {
        return String.format("Hello %s! Todays special is '%s' with '%s'.", customer, special, sauce);
    }
}
