package com.dellemc.minimal.springservices.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
// @EnableWebMvc
public class DefaultExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler({ Exception.class })
    @ResponseBody
    public ResponseEntity<?> handleAnyException(Exception e, HttpStatus status, HttpHeaders headers) {
        return errorResponse(e, status, headers);
    }

    protected ResponseEntity<?> errorResponse(Throwable throwable, HttpStatus status, HttpHeaders headers) {
        if (null != throwable) {
            LOG.error("error caught: " + throwable.getMessage(), throwable);
            ErrorMessage errorMessage = new ErrorMessage(throwable);
            ResponseEntity<ErrorMessage> responseEntity = new ResponseEntity<>(errorMessage, headers, status);
            return responseEntity;
        } else {
            LOG.error("unknown error caught in RESTController, {}", status);
            ResponseEntity<String> responseEntity = new ResponseEntity<>("unknown error caught in RESTController", headers, status);
            return responseEntity;
        }
    }
}
