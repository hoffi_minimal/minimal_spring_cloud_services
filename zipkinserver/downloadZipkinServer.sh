#!/bin/bash

#a="/$0"; a=${a%/*}; a=${a:-.}; a=${a#/}/; BINDIR=$(cd $a; pwd)

if [ `basename $PWD` != "zipkinserver" ]; then
	pushd zipkinserver
else
    pushd .
fi

curl -sSL https://zipkin.io/quickstart.sh | bash -s
chmod 755 zipkin.jar
popd
