package com.dellemc.minimal.circuitbreaker.sources;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import com.dellemc.minimal.circuitbreaker.channels.Tier1Channels;
import com.dellemc.minimal.circuitbreaker.common.dto.MessageDTO;

import contracts.baseclasses.sources.SourceTier1TestBaseClass;

@ActiveProfiles({ "tier1" })
@TestPropertySource(properties = { "spring.application.name=circuitbreakers_tier1", "app.businessLogic.tier=tier1",
        "eureka.client.enabled=false", "spring.cloud.config.enabled=false", "management.endpoints.enabled-by-default=false",
        "management.endpoints.web.exposure.exclude=\"*\"" })
//@JsonTest // don't know but test does not start at all with this
@EnableCircuitBreaker
@AutoConfigureCache
@AutoConfigureJson
@AutoConfigureJsonTesters
@AutoConfigureMessageVerifier
@SpringBootTest
class SourceTier1ContractTest extends SourceTier1TestBaseClass {

    @Autowired
    @Qualifier(Tier1Channels.OUTPUT)
    private MessageChannel tier1OutputMessageChannel;

    @Test
    public void contractMessagingTestForTier1() {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.id = 42;
        messageDTO.message = "Hoffi";
        messageDTO.modifiers = "modifiedByHoffisTest";

        Map<String, Object> headers = new HashMap<>();
        headers.put("id", "TestTraceId");
        headers.put("baggage-bpn", "testbpn");
        headers.put("baggage-bpid", "42");
        headers.put("baggage-succ", "nexttest");
        headers.put("contentType", "application/json");

        super.messageVerifier.send(messageDTO, headers, Tier1Channels.INPUT);

    }
}
