package contracts.baseclasses.sources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.MessageVerifier;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;

import com.dellemc.minimal.circuitbreaker.CircuitBreakerApplication;

@SpringBootTest(classes = CircuitBreakerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureMessageVerifier
public abstract class SourceTier1TestBaseClass {

    @Autowired
    public MessageVerifier messageVerifier;

}
