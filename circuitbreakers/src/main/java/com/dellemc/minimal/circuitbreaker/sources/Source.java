package com.dellemc.minimal.circuitbreaker.sources;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.dellemc.minimal.circuitbreaker.channels.SourceChannels;
import com.dellemc.minimal.circuitbreaker.common.dto.MessageDTO;
import com.dellemc.minimal.circuitbreaker.zipkinsleuthlogging.CustomBaggage;
import com.dellemc.minimal.circuitbreaker.zipkinsleuthlogging.MDCLocal;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.exception.HystrixTimeoutException;

@Profile({ "source" })
@Component
public class Source {
    private static Logger log = LoggerFactory.getLogger(Source.class);

    @Value("${app.businessLogic.tier}")
    private String tier;
    @Value("${app.info.instance_index}")
    private String instanceIndex;

    @Autowired
    private CustomBaggage customBaggage;

    private static AtomicInteger fakeId = new AtomicInteger();

    // as both: injecting the Channel and only @SendTo annotation on the producer-method
    // let the send happen where the @HystrixCommand cannot catch and extract
    // we have to directly inject and use the MessageChannel on producing messages
    //    @Autowired
    //    private SourceChannels sourceChannels;
    @Autowired
    @Qualifier(SourceChannels.OUTPUT)
    private MessageChannel sourceOutputMessageChannel;

    // publishing to an exchange with no routable queue will never get an exception (only an async return callback)
    // @Scheduled(fixedDelay = 2500, initialDelay = 500) // commented out in favour of the RefreshableScheduler in this same package
    @HystrixCommand(fallbackMethod = "fallbackTimerMessageSource")
    @SendTo(SourceChannels.OUTPUT) // redundant here, as we directly use the injected Message Channel
    @NewSpan("SourceSendTo")
    public void timerMessageSource() {
        String newChunk = new Object() {}.getClass().getEnclosingMethod().getName();
        MDCLocal.startChunk(newChunk);
        try {
            Integer newFakeId = fakeId.incrementAndGet();
            customBaggage.startTrace("myCustomBpName", newFakeId, "sink1|somethingElse");

            MessageDTO messageDTO = new MessageDTO();
            messageDTO.id = newFakeId;
            messageDTO.message = "fromSource";

            log.info("[{}]Produced: '{}'", instanceIndex, messageDTO);
            sourceOutputMessageChannel.send(MessageBuilder.withPayload(messageDTO).build());
        } finally {
            MDCLocal.endChunk(newChunk);
            log.info("CHECK if chunk is removed");
        }
    }

    @NewSpan("SourceSendToFallback")
    public void fallbackTimerMessageSource(Throwable t) {
        if (t instanceof HystrixTimeoutException) {
            log.error("Hystrix fallback. The BusinessLogic exceeded the execution.isolation.thread.timeoutInMilliseconds");
        } else {
            log.error("Hystrix fallback. Message couldn't be send. The original exception was", t);
        }
    }
}
