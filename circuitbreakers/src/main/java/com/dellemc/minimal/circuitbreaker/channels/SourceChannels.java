package com.dellemc.minimal.circuitbreaker.channels;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface SourceChannels {

    String OUTPUT = "minimal-SourceTo1";

    @Output(OUTPUT)
    MessageChannel sourceOutput();
}