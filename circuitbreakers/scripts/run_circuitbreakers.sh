#!/bin/bash

justEcho=false
if [[ "$1" == dry* ]] || [[ "$2" == dry* ]]; then # just echo don't start
  justEcho=true
  shift
fi
additionalProfiles="" # comma separated without spaces
if [ ! -z "$1" ]; then
  additionalProfiles=",$1"
fi

# call this script from root of circuitbreakers subproject
declare -a theCmds=()
declare -i port=8079
for tier in sink tier2 tier1 source ; do
  ((port+=1))
  theCmd="SERVER_PORT=$port SPRING_PROFILES_ACTIVE=local,$tier$additionalProfiles build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar --spring.application.name=circuitbreakers_$tier --app.businessLogic.tier=$tier --logging.file=circuitbreakers_$tier.log"
  echo $theCmd
  if [ ! "$justEcho" = true ]; then
    theCmds+=("$theCmd")
  fi
done

if [ ! "$justEcho" = true ]; then
  echo -----------------------------------
  echo executing...
fi

for ((i = 0; i < ${#theCmds[@]}; i++)); do
  echo "${theCmds[$i]}"
  eval ${theCmds[$i]} &
  sleep 5000
done
