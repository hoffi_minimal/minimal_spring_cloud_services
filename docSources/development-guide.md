Development Guide for spring-cloud services
==========================

This project contains simple example projects which - combined - are showcasing the utilisation of spring-cloud services \
and spring-cloud-sleuth distributed log metadata in combination with zipkin distributed tracing.

In depth documentation on spring-cloud in general can be found under:

- http://projects.spring.io/spring-cloud/
- https://projects.spring.io/spring-cloud/spring-cloud.html
- http://cloud.spring.io/spring-cloud-static/Finchley.RC2/
- https://cloud.spring.io/spring-cloud-netflix/

Information on configuring, operating, developing of PCF Spring-Cloud-Services Tile services:

- https://docs.pivotal.io/spring-cloud-services/1-5/common/

The examples schowcase the following spring-cloud projects:

- spring-cloud-config (distributed/versioned application.properties configuration)
- spring-cloud-netfix-eureka (service registry and discovery)
- spring-cloud-hystrix (Hystrix clients can be built with a simple annotation-driven method decorator)
- Hystrix dashboard with declarative Java configuration
- Turbine (gathering of Hystrix data from multiple apps for displaying in one single hystrix dashboard)
- spring-cloud-sleuth (distributed tracing)
- zipkin (tracing information server and ui)
- spring-cloud-stream (lightweight event-driven microservices)

---

**_Demo App uses RabbitMQ Messaging server as communication infrastructure_**

---

## Quick Start

to compile, in the multiproject root folder type

```
./gradlew clean build test
```

to just recompile a subproject `cd <subfolder> ; ../gradlew clean build test`


if you want to also push tracing data to a zipkinserver download the zipkin.jar first with `./zipkinserver/downloadZipkinServer.sh`

`./scripts/10_createServices.sh <https://<env>.<sysDomain> <serviceInstancesNamePrefix> <optional:pathToConfigServerJsonConfigFile>`

`./scripts/20_compile.sh <serviceInstancesNamePrefix>`

`./scripts/30_cf_push.sh <bind|nobind> <serviceInstancesNamePrefix> <optional:pathToCircuitbreakerClient.jar>`

There are also startscripts for the circuitbreakers_* apps inside `./circuitbreakers/scripts/run_circuitbreakers.sh` from which you can see how to push manually (beware of setting the right spring cloud profiles and include e.g. the `sleuth` spring profile. see **Available Spring Profiles for curcuitbreakers demo apps** section later in this document for a list of existing spring profiles for circuitbreakers app.

For a list of **REST endpoints** to see something look at the section near the end of this document with the same name.


## folder structure

```
.
|-- README.md
|-- build/                            (gradle build generated artefacts)
│   `-- libs/                         (gradle generated jar files)
|-- build.gradle                      (gradle master root project build file)
|-- buildExt.gradle                   (imported project specific gradle parts)
|-- buildfiles/                       (imported gradle custom functionality)
|-- environments.config               (environment specific build configuration)
|-- gradle.properties                 (global gradle configuration)
|-- settings.gradle                   (gradle multi projects configuration)
|
|-- scripts/                          (convenience helper scripts)
│   |-- 10_createServices.sh
│   |-- 20_compile.sh
│   `-- 30_cf_push.sh
|
|-- circuitbreakers/                  (sending/receiving microservices App)
│   |-- build.gradle                  (subproject gradle build file)
│   |-- docs/                         (subproject specific docs)
│   │   |-- Channels.svg
│   │   `-- DrawIo.xml
│   |-- scripts/                      (helper scripts)
│   │   |-- fixedDelay.sh             (alter rate of message generation)
│   │   `-- run_circuitbreakers.sh    (start sending/intermediate/receiving layer Apps)
│   `-- src/
│       |-- main/
│       │   |-- java/
│       │   `-- resources/
│       `-- test/
│           `-- java/
|
|-- configclient/                     (simple client that reads config from configserver)
│   |-- build.gradle                  (subproject gradle build file)
│   |-- generated/                    (gradle generated convenience scripts)
│   |-- scripts/                      (helper scripts)
│   `-- src/
│       |-- main/
│       │   |-- java/
│       │   `-- resources/
│       `-- test/
│           |-- java/
│           `-- resources/
|
|-- configserver\                     (standalone config server for local testing)
│   |-- build.gradle                  (subproject gradle build file)
│   |-- generated/                    (gradle generated convenience scripts)
│   `-- src/
│       |-- main/
│       │   |-- java/
│       │   `-- resources/
│       `-- test/
│           `-- java/
|
|-- docSources/                       (Multiproject documentation)
│   |-- 00README.txt
│   |-- deployment-guide.md
│   |-- development-guide.md
│   |-- release-notes.md
│   |-- template.html
│   `-- user-guide.md
|
|-- hystrixdashboard/                 (Hystrix Dashboard App)
│   |-- build.gradle                  (subproject gradle build file)
│   `-- src/
│       |-- main/
│       │   |-- java/
│       │   `-- resources/
│       `-- test/
│           `-- java/
|
|-- hystrixturbine/                   (Turbine Hystrix Data Server)
│   |-- build.gradle                  (subproject gradle build file)
│   `-- src/
│       |-- main/
│       │   |-- java/
│       │   `-- resources/
│       `-- test/
│           `-- java/
|
|-- serviceregistryserver/            (standalone service registry for local testing)
│   |-- build.gradle                  (subproject gradle build file)
│   |-- scripts/
│   │   `-- startServiceRegistry.sh
│   `-- src/
│       |-- main/
│       │   |-- java/
│       │   `-- resources/
│       `-- test/
│           `-- java/
|
`-- zipkinserver/                     (zipkin open tracing server and ui)
    |-- build.gradle                  (subproject gradle build file)
    |-- downloadZipkinServer.sh
    |
    `-- zipkin.jar
```

## common package structure of sub-projects

```
.src/main/java/com/bosch/de/bics/rabbitmq/smoketests
|-- bootconfighelpers/  (helpers for dealing with spring @Configuration classes)
|-- bootconfigs/        (spring @Configuration classes)
|-- common/
|   `-- dto/            (Data Transfer Objects)
|-- exceptions/         (Exception classes)
|-- helpers/            (general helper stuff)
|-- ws/                 (REST spring `@RestControllers` with `@RequestMappings`)
|
`-- XXXApplication.java (Main spring boot class)
```

## configuring of target environments

Target environments can be configured in `environments.config` and can be targeted for a gradle build with the `-Penv=<name>` parameter, \
these are used by e.g. the `rootfolder/buildfiles/buildCf.gradle` imported gradle build file. Environments.config might look like this:

```
  pez {
      spring {
        springProfiles = "cassandra,mongo,rabbit,redis"
      }
      cloudfoundry {
        target = "https://api.run.pez.pivotal.io"
        baseURL = "run.pez.pivotal.io"
        organization = "pivot-dhoffmann"
        space = "development"
        trustSelfSignedCerts = true
        username = project.getSecuritySensitiveProperty('cfPezDhoffmannDevUsernameDeveloper', '<username>')
        password = project.getSecuritySensitiveProperty('cfPezDhoffmannDevPasswordDeveloper', '<password>')
      }
      application {
        instances = 1
        memory = 512
        diskQuota = 512
      }
      services {
        serviceMap {
          cassandra = [name:'Cassandra', service:'p-cassandra', plan:'multi-tenant']
          mongo =     [name:'MongoDB',   service:'p-mongodb',   plan:'development']
          mysql =     [name:'MySQL',     service:'p-mysql',     plan:'100mb-dev']
          mysql2 =    [name:'MySQL2',    service:'p-mysql',     plan:'100mb-dev']
          rabbit =    [name:'RabbitMQ',  service:'p-rabbitmq',  plan:'standard']
          redis =     [name:'Redis',     service:'p-redis',     plan:'shared-vm']
        }
        bindings = ['cassandra', 'mongo', 'rabbit', 'redis']
      }
  }
```

Every `project.getSecuritySensitiveProperty(<key>, 'description if key is missing')` parameter will be fetched from
your `~/.gradle/gradle.properties` file, as it is considered to be security sensitive and therefore should not be in clear
text somewhere in the source or build artefacts.

The `serviceMap` under the `services` section describes all services that exist within this targets org/space.
This `serviceMap` does not have to be exhaustive.

The `bindings` array under the `services` section defines which of the services the app(s) should be bound to (`cf bind-service ...`)

You can can deploy/run the app(s) by using the following gradle targets (described in more detail further on below):

`gradle -Penv=<name> showCfCmds`  
or  
`gradle -Penv=<name> showBootRunCommand`  
or directly run it locally by  
```
SPRING_PROFILES_ACTIVE=mongo,localmongo,rabbit,localrabbit,redis,localredis gradle -x test bootRun
``` 
or by pure java  
```
SPRING_PROFILES_ACTIVE=rabbit,localrabbit,redis,localredis java -jar build/libs/minimal_spring_cloud_services-0.5.0-SNAPSHOT.jar
```  

There is also a `gradle generateCfCmds` target which will generate fully operational deploy scripts to the `generated/` folder.

Possible gradle properties for `gradle showCfCmds` and `gradle generateCfCmds` are:
  
- `-Penv=<env>`  
&nbsp;&nbsp;&nbsp;&nbsp; (references a environment from `environments.config`, `dev`by default)
- `-PwithServices=<true|false>`  
&nbsp;&nbsp;&nbsp;&nbsp; (if `cf create-service` statements should be generated. defaults to `false`)
- `-PwithBindings=<true|false>`  
&nbsp;&nbsp;&nbsp;&nbsp; (if `cf bind-service` statements should be generated. defaults to `true`)
- `-PmaskPasswords=<true|false>`  
&nbsp;&nbsp;&nbsp;&nbsp; (if the password should be generated or hidden by `<password>`, defaults to `true`)
- `-PappNamePrefix=<PREFIX|empty>`  
&nbsp;&nbsp;&nbsp;&nbsp; (a PREFIX for the appName. AppName is `build.gradle`s `ext.baseName`, defaults to `""`)
- `-PappNamePostfix=<POSTFIX|empty>`  
&nbsp;&nbsp;&nbsp;&nbsp; (a POSTFIX for the appName. AppName is `build.gradle`s `ext.baseName`, defaults to `""`)
- `-PspringProfilesActive=<prof1,prof2,...>`  
&nbsp;&nbsp;&nbsp;&nbsp; (`cf set-env SPRING_PROFILES_ACTIVE ...`, default to the `spring.springProfiles` of `environments.config`)
- `-PjarDir=<pathToJarDir>`  
&nbsp;&nbsp;&nbsp;&nbsp; (path to dir with the build jar file, defaults to gradles `build/libs`)
- `-PtargetOS=<unix|win>`  
&nbsp;&nbsp;&nbsp;&nbsp; (target operating system, defaults to `unix`)

## Requirements

- IntelliJ IDEA, Eclipse or vsCode Editor
- Java 8+
- Gradle 4.10+
- spring-boot 2.1+
- spring-cloud Grenwhich+
- rabbitMQ server
- Cloud foundry CLI

## How to setup project with IntelliJ IDEA

```
git clone <path.to.project>
cd minimal_spring_cloud_services
gradle idea
```

Import it to IntelliJ IDEA.

## How to setup project with Eclipse

```
git clone <path.to.project>
cd minimal_spring_cloud_services
gradle eclipse
```

Import it to Eclipse

* Files->import...->Gradle (STS)
* choose `minimal_spring_cloud_services` as root folder
* click `Build Model`
* check `Enable dependency management`
* uncheck `Create resource filters`
* uncheck `Create working sets based on root project name`

## How to run project locally

First you have to create or edit a `gradle.properties` in `$USER_HOME/.gradle/gradle.properties` with the following content:  
(the keys are referenced in your `buildfiles/buildCf.gradle` file in the `cloudfoundry {` section)

```
cfUsername=<your cloud foundry user>
cfPassword=<your cloud foundry pass>
```

### run_circuitbreakers.sh

In subproject `circuitbreakers/scripts/` there is a `run_circuitbreakers.sh script helping you to fire up all 4 tier apps and giving them
the right spring profile and `spring.application.name`.

If you give a first parameter `dry` to the run_circuitbreakers.sh script it just prints out the commands so you can start them in different shells.

e.g.

```
$ cd circuitbreakers
$ scripts/run_circuitbreakers.sh dry
SERVER_PORT=8080 SPRING_PROFILES_ACTIVE=local,sink \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
    --spring.application.name=circuitbreakers_sink --app.businessLogic.tier=sink \
    --logging.file=circuitbreakers_sink.log
SERVER_PORT=8081 SPRING_PROFILES_ACTIVE=local,tier2 \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
    --spring.application.name=circuitbreakers_tier2 --app.businessLogic.tier=tier2 \
    --logging.file=circuitbreakers_tier2.log
SERVER_PORT=8082 SPRING_PROFILES_ACTIVE=local,tier1 \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
    --spring.application.name=circuitbreakers_tier1 --app.businessLogic.tier=tier1 \
    --logging.file=circuitbreakers_tier1.log
SERVER_PORT=8083 SPRING_PROFILES_ACTIVE=local,source \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
    --spring.application.name=circuitbreakers_source --app.businessLogic.tier=source \
    --logging.file=circuitbreakers_source.log
```


### Available Spring Profiles for curcuitbreakers demo apps

- **local**       (if not running in the cloud for configuring peer URIs, users, passwords)
- **cloud**       (if running in cloudfoundry - autoactivated if pushed to PCF)
- **source**      (start circuitbreaker app as source which just sends)
- **tier1**       (start circuitbreaker app as tier1 which receives from source and sends to tier1)
- **tier2**       (start circuitbreaker app as tier 2 which receives from tier1 and sends to tier2)
- **sink**        (start circuitbreaker app as sink which just receives from tier2)

### Run the App locally

To find out the settings you will need to run the application from your IDE run the following command:

```
cd minimal_spring_cloud_services
gradle showBootRunCommand -Penv=dev
========= Spring boot run command - START =========
java -jar build/libs/minimal_spring_cloud_services-0.5.0-SNAPSHOT.jar --spring.profiles.active=<activeProfilesFromSrc/main/resources/application.yml>
e.g.:
java -jar build/libs/minimal_spring_cloud_services-0.5.0-SNAPSHOT.jar --spring.profiles.active=rabbit,localrabbit,mongo,localmongo
========= Spring boot run command - END ===========
```

Further more you can run it directly from gradle, e.g.:

```
SPRING_PROFILES_ACTIVE=rabbit,localrabbit,mongo,localmongo gradle -x test bootRun
```

For circuitbreakers demo apps there is a convenience scripts firing up source, tiers and sink apps on different ports with different names (including a dry option to just print out the app start commands):

```
$ circuitbreakers/scripts/run_circuitbreakers.sh sleuth dry

SERVER_PORT=8080 SPRING_PROFILES_ACTIVE=local,sink \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
       --spring.application.name=circuitbreakers_sink

SERVER_PORT=8081 SPRING_PROFILES_ACTIVE=local,tier2 \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
       --spring.application.name=circuitbreakers_tier2

SERVER_PORT=8082 SPRING_PROFILES_ACTIVE=local,tier1 \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
       --spring.application.name=circuitbreakers_tier1

SERVER_PORT=8083 SPRING_PROFILES_ACTIVE=local,source \
    build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar \
       --spring.application.name=circuitbreakers_source
```

### Run the App on cloudfoundry PaaS

if you want to test out spring cloud config server, you have to have a git repo which is accessible from your cloudfoundry app (e.g. https://gitlab.thalesdigital.io).

Inside this repo you have to have properties or yaml spring cloud configuration files (as in your src/main/resources/application.(properties|yml) spring config files) which have **the same file basename as the name of the app as you pushed it to cloudfoundry**.

You then need to configure your spring cloud config server as described in https://docs.pivotal.io/spring-cloud-services/1-5/common/config-server/configuring-with-git.html to use this git repo.

minimal circuitbreakers apps of this demo use the following config value which will be logged after startup:

```
my:
  test:
    property: "value comes from config server git repo"
```

#### provision and configure Spring Cloud Services Config Server in cloudfoundry

find detailed instructions on https://docs.pivotal.io/spring-cloud-services/1-5/common/

#### provision and configure Spring Cloud Services Eureka Service Registry in cloudfoundry

find detailed instructions on https://docs.pivotal.io/spring-cloud-services/1-5/common/

#### provision and configure Spring Cloud Services Hystrix Dashboard in cloudfoundry

find detailed instructions on https://docs.pivotal.io/spring-cloud-services/1-5/common/

### Turbine Hystrix Data Gathering Server

tbd

#### push custom 

tbd

#### push Application to Cloudfoundry

You may run the following custom gradle commands to get a hint on the necessary cf cli commands:

```
$ cd minimal_spring_cloud_services
$ gradle showCfCmds -Penv=<envFrom_environments.config>
Environment is set to "dev" (e.g. gradle -Penv=dev clean build)
loadEnvDefs reading file:/Users/hoffmd9/DBs/git/Bosch/minimal_spring_cloud_services/environments.config
CF CloudController (CC) target for -Penv=dev is http://api.run.pivotal.io
:showCfCmds

 gradle showCfCmds -Penv=dev -PwithServices=false -PwithBindings=true -PmaskPasswords=true \  
                             -PappNamePrefix= -PappNamePostfix= \  
                             -PspringProfilesActive=mysql,rabbit,redis  \
                             -PjarDir=build/libs -PtargetOS=unix

========= Cloud foundry CLI deploy commands - START ========= for minimal_spring_cloud_services
cf api https://api.local.pcfdev.io --skip-ssl-validation
cf login -u admin -p "<password>" -o "pcfdev-org" -s "pcfdev-space" --skip-ssl-validation

cf push minimal_spring_cloud_services -k 256M -m 256M -i 1 -p build/libs/minimal_spring_cloud_services-0.5.0-SNAPSHOT.jar --no-start
cf bind-service minimal_spring_cloud_services MySql
cf bind-service minimal_spring_cloud_services RabbitMQ
cf bind-service minimal_spring_cloud_services Redis
cf set-env minimal_spring_cloud_services VERSION "0.5.0-SNAPSHOT"
cf set-env minimal_spring_cloud_services BUILD_TIME "Sun May 22 14:29:33 CEST 2016"
cf set-env minimal_spring_cloud_services PUSHED_BY_USER "hoffmd9"
cf set-env minimal_spring_cloud_services GIT_REPO "git@gitlab.thalesdigital.io:paas-pcf/minimal_spring_cloud_services.git"
cf set-env minimal_spring_cloud_services SPRING_PROFILES_ACTIVE "mysql,rabbit,redis"
cf restart minimal_spring_cloud_services
========= Cloud foundry CLI deploy commands - END ===========

BUILD SUCCESSFUL
```

There is a corresponding gradle task which generateds operational unix shellscript and windows bat file for deployment.

`gradle generateCfCmds` with identical gradle Properties as described above.

Scripts are generated into `generated/` folder.

- generated/`deploy-<appname>-allInOneApp.bat`
- generated/`deploy-<appname>-allInOneApp.sh`
- generated/`deploy-<appname>-singleAppPerService.bat`
- generated/`deploy-<appname>-singleAppPerService.sh`

## REST endpoints

### common actuator endpoints

All spring boot apps are instrumented with (unsecured) complete set of actuator endpoints. You can get a list of all available actuator endpoints by asking the app itself for it with:

[https://appurl:port/actuator](http://localhost:port/actuator)

if running on cloudfoundry, take the app url without the port oviously.

### configserver

[https://appurl:8888](http://localhost:8888)

### Netflix Eureka service registry and discovery server

[https://appurl:8761](http://localhost:8761)

### Netflix Hystrix Dashboard

[https://appurl:7979](http://localhost:7979)

you can see the raw hystrix data which is emmited on [https://appurl:port/actuator/hystrix.stream](http://localhost:port/actuator/hystrix.stream) <br/>
(for spring 1.x just [https://appurl:port/hystrix.stream](https://appurl:port/hystrix.stream))

### Turbin Hystrix Data Gathering Server

[https://appurl:7978](http://localhost:7978/turbine.stream)

### zipkin server

[https://appurl:9411](http://localhost:9411)

### circuitbreakers demo App (multitiered)

see above


## Documentation

Documentation is located in the `/docSources/` folder and written in [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
The Markdown documents will be compiled to PDFs (**generated into** `docs/` **dir**):

```
gradle docs
```

## Appendix

none