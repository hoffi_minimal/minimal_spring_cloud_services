circuitbreakers multi-tiered demo apps
==========================

the multi-tiered demo apps are in subproject `circuitbreakers`.


---

## TL;DR

before you run locally, beware to have the infrastructure components setup and running

```
$ rabbitmq-server &
$ pushd serviceregistryserver/
$ ./build/libs/minimal_serviceregistryserver-1.0.1.RELEASE.jar &
$ popd

$ pushd zipkinserver/
$ ./downloadZipkinServer.sh
$ ./zipkin.jar &
$ popd

$ pushd hystrixdashboard/
$ ./build/libs/minimal_hystrixdashboard-1.0.1.RELEASE.jar &
$ popd

$ pushd hystrixturbine/
$ ./build/libs/minimal_hystrixturbine-1.0.1.RELEASE.jar &
$ popd
 
$ # optional
$ pushd configserver/
$ ./build/libs/minimal_configserver-1.0.1.RELEASE.jar &
$ popd
```

you can run them locally by calling `scripts/run_circuitbreakers.sh` or with giving `dry` as first parameter it prints out the commands it executes

```
$ cd circuitbreakers
$ scripts/run_circuitbreakers.sh dry
SERVER_PORT=8080 SPRING_PROFILES_ACTIVE=local,sink build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar --spring.application.name=circuitbreakers_sink --app.businessLogic.tier=sink --logging.file=circuitbreakers_sink.log
SERVER_PORT=8081 SPRING_PROFILES_ACTIVE=local,tier2 build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar --spring.application.name=circuitbreakers_tier2 --app.businessLogic.tier=tier2 --logging.file=circuitbreakers_tier2.log
SERVER_PORT=8082 SPRING_PROFILES_ACTIVE=local,tier1 build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar --spring.application.name=circuitbreakers_tier1 --app.businessLogic.tier=tier1 --logging.file=circuitbreakers_tier1.log
SERVER_PORT=8083 SPRING_PROFILES_ACTIVE=local,source build/libs/minimal_circuitbreakers-1.0.1.RELEASE.jar --spring.application.name=circuitbreakers_source --app.businessLogic.tier=source --logging.file=circuitbreakers_source.log
```

# spring cloud stream channels

You don't need to 'only' use spring streams `@Source`, `@Processor` and `@Sink` annotations for channels.

You can define your own, as these are just interfaces and string identifiers.

Have a look in package `channels` as I have defined custom channels there, e.g.:

```
public interface Tier1Channels {

    String INPUT = "minimal-SourceTo1";
    String OUTPUT = "minimal-1To2";

    @Input(INPUT)
    SubscribableChannel tier1Input();

    @Output(OUTPUT)
    MessageChannel tier1Output();
}
```

the INPUT and OUTPUT strings have to match the `application.(yml|properties)` like so:

```
spring:
  profiles: tier1
  cloud:
    stream:
      bindings:
        minimal-SourceTo1: # has to match channels.Tier1Channels.INPUT
          group: tier1s
          destination: minimal-SourceTo1
          contentType: application/json
        minimal-1To2: # has to match channels.SourceChannels.OUTPUT
          destination: minimal-1To2
          contentType: application/json
```

# Customa Baggage, Custom Log Fields and Tracing with Zipkin

These three things should be distinguished very carefully:

## Custom Baggage

**Custom Baggage** are fields that travel along with the downstream calls via http, messaging or any spring-integration instrumentation available in the classpath.

It is clever to have an own bean doing so. In this project this is `zipkinsleuthlogging.CustomBaggage.class`

since spring-cloud-sleuth 2.1 (Greenwich release train) these fields can also be put in slf4j MDC context.

For putting extra things like MDC in the log pattern, I recommend not to define a total own log pattern, but do like spring cloud sleuth and only
override the `level` field in `application.(yml|properties` of a dependant root project that any of your app consumes.

```
logging:
  pattern:
      # from org.springframework.cloud.sleuth.autoconfig.TraceEnvironmentPostProcessor
    level: "%5p [${spring.zipkin.service.name:${spring.application.name:-}},%X{X-B3-TraceId:-},%X{X-B3-SpanId:-},%X{X-Span-Export:-}] [%X{bpn:-},%X{bpid:-},%X{succ:-}] [%X{chunk:-}]"
```

For this to work you have to define your Custom Baggage fields and those that should be put in slf4j MDC in `application.(yml|properties) like so:

```
spring:
  sleuth:
    sampler:
      probability: 1.0
    annotation:
      enabled: true
    baggage-keys: # match CustomSlf4jCurrentTraceContext static Strings
    - bpn
    - bpid
    - succ
    log:
      slf4j:
        whitelisted-mdc-keys:
        - bpn
        - bpid
        - succ
```

## Custom MDC Log Fields

On top of Custom Baggage - which travels with the request downstream - an app or part of it can decide to put local key values inside MDC context.
This custom data should be uniform over all Microservices and the fields (and therefore the log-pattern) should not be different for *all* of 
your microservices.

**Custom MDC Log Fields** are not propagated to downstream apps. They are just there to be put into logs in a coordinated way.

It is clever to have an own bean doing so. In this project this is `zipkinsleuthlogging.MDCLocal.class`

## Sleuth Zipkin tracing Tags, Annotations and custom spans

**Sleuth Zipkin tracing Tags and Annotations** are a way to give Spans a searchable and visible name in Zipkin server
and makes searching for them and visualisation more conveninent.

**Custom spans** devide the root span within one app call in meaningfull hierarchical subspans, which - if send to zipkin - also provide
more fine granular tracing and profiling insights to the call overall.


## all three together



